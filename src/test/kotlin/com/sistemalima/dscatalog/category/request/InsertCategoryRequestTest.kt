package com.sistemalima.dscatalog.category.request

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class InsertCategoryRequestTest {

    @Test
    fun `must transform request into entity`() {
        // cenario

        val request = InsertCategoryRequest(
            name = "Eletronicos"
        )

        // ação
        val category = request.toModel()

        // assertivas
        Assertions.assertEquals(category, request.toModel())
        Assertions.assertEquals(category.name, request.name)
    }
}
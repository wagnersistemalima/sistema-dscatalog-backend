package com.sistemalima.dscatalog.category.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.response.FindByIdCategoryResponse
import com.sistemalima.dscatalog.category.service.FindByIdCategoryService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.util.UriComponentsBuilder

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class FindByIdCategoryControllerTest {

    @field:Autowired
    lateinit var mockmvc: MockMvc

    @field:Autowired
    lateinit var objectMapper: ObjectMapper

    @field:MockBean   // usar quando a classe carrega o contexto
    lateinit var findByIdCategoryService: FindByIdCategoryService

    private val category = Category(
        id = 1L,
        name = "Eletronico"
    )

    @Test
    fun `deve retornar 200 ok, e no corpo da resposta um response de category`() {
        // cenario

        val idCategoryValid = 1L
        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_BY_ID + "/{id}").buildAndExpand(idCategoryValid).toUri()
        val response = FindByIdCategoryResponse(category)
        // ação

        // comportamento
        Mockito.`when`(findByIdCategoryService.findById(any(), any())).thenReturn(response)

        mockmvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().json(toJson(response)))

        // assertivas
    }

    @Test
    fun `deve retornar 404 not found`() {
        // cenario

        val idCategoryInValid = 5000L
        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_BY_ID + "/{id}").buildAndExpand(idCategoryInValid).toUri()

        // ação

        // comportamento
        Mockito.`when`(findByIdCategoryService.findById(any(), any())).thenThrow(ResourceNotFoundException("not found"))

        mockmvc.perform(MockMvcRequestBuilders.get(uri).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isNotFound)

        // assertivas
    }

    private fun toJson(response: FindByIdCategoryResponse): String {
        return objectMapper.writeValueAsString(response)
    }
}
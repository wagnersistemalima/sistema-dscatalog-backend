package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.service.DeleteCategoryService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.util.UriComponentsBuilder

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class DeleteCategoryControllerTest {

    @field:Autowired
    lateinit var mockMvc: MockMvc

    @field:MockBean
    lateinit var deleteCategoryService: DeleteCategoryService

    @Test
    fun `deve retornar no-content 204 ao deletar uma categoria pelo id`() {
        // cenario
        val idCategoryValid = 1L

        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_DELETE_BY_ID + "/{id}")
            .buildAndExpand(idCategoryValid)
            .toUri()

        // ação

        mockMvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isNoContent)

        // assertivas
    }

    @Test
    fun `deve retornar no-content 404 ao tentar deletar uma categoria pelo id que nao existe`() {
        // cenario
        val idCategoryNaoExiste = 5000L

        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_DELETE_BY_ID + "/{id}")
            .buildAndExpand(idCategoryNaoExiste)
            .toUri()

        // ação

        // comportamento
        Mockito.`when`(deleteCategoryService.delete(any(), any()))
            .thenThrow(ResourceNotFoundException("Entity not found"))

        mockMvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isNotFound)

        // assertivas
    }
}
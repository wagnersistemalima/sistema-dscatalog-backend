package com.sistemalima.dscatalog.category.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.response.FindAllCategoryResponse
import com.sistemalima.dscatalog.category.service.FindAllCategoryService
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.net.URI

@ExtendWith(SpringExtension::class)
@SpringBootTest
@AutoConfigureMockMvc
class FindAllCategoryControllerTest {

    @field:Autowired
    lateinit var mockMvc: MockMvc

    @field:Autowired
    lateinit var objectMapper: ObjectMapper

    @field:MockBean // usar quando a classe de texte carrega o contexto -> @SpringBootTest
    lateinit var findAllCategoryService: FindAllCategoryService

    private val uri = URI(APIConstants.CATEGORY_GET_ALL)

    @Test
    fun `should return a paged list of categories`() {
        // cenario

        val category = getFactoryCategory()
        val list = PageImpl(listOf(category))
        val response = list.map { FindAllCategoryResponse(category) }

        // comportamento
        Mockito.`when`(findAllCategoryService.findAllPage(any(), any())).thenReturn(response)

        // ação

        mockMvc.perform(MockMvcRequestBuilders.get(uri))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.content().json(toJson(response)))

        // assertivas
        Mockito.verify(findAllCategoryService).findAllPage(any(), any())
        assertNotNull(response)
    }

    private fun getFactoryCategory(): Category {
        val category = Category(
            id = 1L,
            name = "Computador"
        )
        return category
    }

    private fun toJson(response: Page<FindAllCategoryResponse>): String {
       return objectMapper.writeValueAsString(response)
    }
}
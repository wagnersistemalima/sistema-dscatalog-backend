package com.sistemalima.dscatalog.category.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.request.InsertCategoryRequest
import com.sistemalima.dscatalog.category.response.InsertCategoryResponse
import com.sistemalima.dscatalog.category.service.InsertCategoryService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.net.URI
import java.util.UUID

@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
@SpringBootTest
class InsertCategoryControllerTest {

    @field:Mock
    private lateinit var insertCategoryService: InsertCategoryService

    @field:Autowired
    private lateinit var mockMvc: MockMvc

    @field:Autowired
    private lateinit var objectMapper: ObjectMapper

    private val uri = URI(APIConstants.CATEGORY_POST)

    @Test
    fun `should return 201 created`() {
        // cenario
        val correlationId = UUID.randomUUID().toString()

        val category = getRequestValid().toModel()
        val response = InsertCategoryResponse(category)

        Mockito.`when`(insertCategoryService.insertCategory(category, correlationId)).thenReturn(response)

        // ação

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .contentType(MediaType.APPLICATION_JSON).content(toJson(getRequestValid())))
            .andExpect(MockMvcResultMatchers.status().isCreated)

        // assertivas

    }

    @Test
    fun `should return 400 when request name is empty`() {
        // cenario

        // ação

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .contentType(MediaType.APPLICATION_JSON).content(toJson(getRequestInvalid())))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        // assertivas

    }

    private fun toJson(request: InsertCategoryRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    private fun getRequestValid(): InsertCategoryRequest {
       return InsertCategoryRequest(
            name = "Eletronics"
        )
    }

    private fun getRequestInvalid(): InsertCategoryRequest {
        return InsertCategoryRequest(
            name = ""
        )
    }

}
package com.sistemalima.dscatalog.category.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.request.UpdateCategoryRequest
import com.sistemalima.dscatalog.category.service.UpdateCategoryService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.util.UriComponentsBuilder

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class UpdateCategoryControllerTest {

    @field:Autowired
    lateinit var mockMvc: MockMvc

    @field:Autowired
    lateinit var objectMapper: ObjectMapper

    @field:MockBean
    lateinit var updateCategoryService: UpdateCategoryService

    @Test
    fun `deve retornar 200 ok`() {
        // cenario
        val idcategoryValid = 1L
        val request = getRequestValid()

        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_BY_ID_UPADATE + "/{id}")
            .buildAndExpand(idcategoryValid)
            .toUri()

        // ação

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .contentType(MediaType.APPLICATION_JSON).content(toJson(request)))
            .andExpect(MockMvcResultMatchers.status().isOk)

        // assertivas
    }

    @Test
    fun `deve retornar 404 not found, quando não achar id category`() {
        // cenario
        val idcategoryInValid = 5000L
        val request = getRequestValid()

        // comportamento
        Mockito.`when`(updateCategoryService.update(any(), any(), any())).thenThrow(ResourceNotFoundException("Entity not found"))

        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_BY_ID_UPADATE + "/{id}")
            .buildAndExpand(idcategoryInValid)
            .toUri()

        // ação

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .contentType(MediaType.APPLICATION_JSON).content(toJson(request)))
            .andExpect(MockMvcResultMatchers.status().isNotFound)

        // assertivas
    }

    @Test
    fun `deve retornar 400 bad request, quando o nome da category request estiver vazio`() {
        // cenario
        val idcategoryInValid = 5000L
        val request = getRequestInvalid()

        val uri = UriComponentsBuilder.fromUriString(APIConstants.CATEGORY_BY_ID_UPADATE + "/{id}")
            .buildAndExpand(idcategoryInValid)
            .toUri()

        // ação

        mockMvc.perform(MockMvcRequestBuilders.put(uri)
            .contentType(MediaType.APPLICATION_JSON).content(toJson(request)))
            .andExpect(MockMvcResultMatchers.status().isBadRequest)

        // assertivas
    }

    private fun toJson(request: UpdateCategoryRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    private fun getRequestValid(): UpdateCategoryRequest {
        return UpdateCategoryRequest(name = "Automoveis")
    }

    private fun getRequestInvalid(): UpdateCategoryRequest {
        return UpdateCategoryRequest(
            name = ""
        )
    }
}
package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.request.UpdateCategoryRequest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.Optional
import java.util.UUID

@ExtendWith(SpringExtension::class)
class UpdateCategoryServiceTest {

    @field:InjectMocks
    lateinit var updateCategoryService: UpdateCategoryService

    @field:Mock
    lateinit var categoryRepository: CategoryRepository

    private val category = Category(
        id = 1L,
        name = "Eletronico"
    )

    @Test
    fun `deve atualizar no banco o nome de uma categoria, quando receber a request`() {
        // cenario
        val idValid = category.id
        val request = UpdateCategoryRequest(name = "Automoveis")
        val correlationId = UUID.randomUUID().toString()
        // ação

        // comportamento
        Mockito.`when`(categoryRepository.findById(idValid!!)).thenReturn(Optional.of(category))

        // assertivas
        Assertions.assertDoesNotThrow { updateCategoryService.update(idValid, request, correlationId) }
        Assertions.assertEquals("Automoveis", category.name)
        Assertions.assertNotNull(category.updateDate)
        Mockito.verify(categoryRepository).save(category)
    }

    @Test
    fun `não deve atualizar no banco o nome de uma categoria, lançar exception ResourceNotFoundException quando não encontrar category`() {
        // cenario
        val idInValid = 5000L
        val request = UpdateCategoryRequest(name = "Automoveis")
        val correlationId = UUID.randomUUID().toString()
        // ação

        // comportamento
        Mockito.`when`(categoryRepository.findById(idInValid)).thenReturn(Optional.empty())

        // assertivas
        Assertions.assertThrows(ResourceNotFoundException::class.java) {updateCategoryService.update(idInValid, request, correlationId)}
        Mockito.verify(categoryRepository, Mockito.times(0)).save(category)
        Assertions.assertNull(category.updateDate)
    }
}
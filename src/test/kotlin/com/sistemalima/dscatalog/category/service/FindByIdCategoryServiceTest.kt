package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.response.FindByIdCategoryResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.Optional
import java.util.UUID

@ExtendWith(SpringExtension::class)
class FindByIdCategoryServiceTest {

    @field:Mock
    lateinit var categoryRepository: CategoryRepository

    @field:InjectMocks
    lateinit var findByIdCategoryService: FindByIdCategoryService

    private val category = Category(
        id = 1L,
        name = "Eletronico"
    )

    @Test
    fun `deve retornar um response de category`() {
        // cenario
        val idExistente: Long? = category.id
        val correlationId = UUID.randomUUID().toString()
        val response = FindByIdCategoryResponse(category)

        // comportamento
        Mockito.`when`(categoryRepository.findById(idExistente!!)).thenReturn(Optional.of(category))

        // ação

        // assertivas
        Assertions.assertDoesNotThrow { findByIdCategoryService.findById(idExistente, correlationId) }
        Mockito.verify(categoryRepository).findById(idExistente)
        Assertions.assertEquals(response, findByIdCategoryService.findById(idExistente, correlationId))

    }

    @Test
    fun `deve retornar uma exception ResourceNotFoundexception status 404, quando nao encontrar uma categoria`() {
        // cenario
        val idNaoExistente = 50000L
        val correlationId = UUID.randomUUID().toString()

        // comportamento
        Mockito.`when`(categoryRepository.findById(idNaoExistente)).thenReturn(Optional.empty())

        // ação

        // assertivas
        Assertions.assertThrows(ResourceNotFoundException::class.java) {findByIdCategoryService.findById(idNaoExistente, correlationId)}
        Mockito.verify(categoryRepository).findById(idNaoExistente)
    }
}
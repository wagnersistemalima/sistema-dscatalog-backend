package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.Optional
import java.util.UUID

@ExtendWith(SpringExtension::class)
class DeleteCategoryServiceTest {

    @field:InjectMocks
    lateinit var deleteCategoryService: DeleteCategoryService

    @field:Mock
    lateinit var categoryRepository: CategoryRepository

    @Test
    fun `deve deletar uma categoria pelo id, quando receber o id da categoria`() {
        // cenario

        val category = getCategory()
        val idCategoryValid = category.id
        val correlationId = UUID.randomUUID().toString()

        // ação

        //comportamento
        Mockito.`when`(categoryRepository.findById(idCategoryValid!!)).thenReturn(Optional.of(category))

        // assertivas
        Assertions.assertDoesNotThrow { deleteCategoryService.delete(idCategoryValid, correlationId) }
        Mockito.verify(categoryRepository).delete(category)
        Mockito.verify(categoryRepository).findById(idCategoryValid)
    }

    @Test
    fun `nao deve deletar uma categoria pelo id, quando receber o id da categoria que nao existe, lancar exception`() {
        // cenario

        val idCategoryInValid = 5000L
        val correlationId = UUID.randomUUID().toString()

        // ação

        //comportamento
        Mockito.`when`(categoryRepository.findById(idCategoryInValid)).thenReturn(Optional.empty())

        // assertivas
        Assertions.assertThrows(ResourceNotFoundException::class.java) {deleteCategoryService.delete(idCategoryInValid, correlationId)}
        Mockito.verify(categoryRepository).findById(idCategoryInValid)
    }

    private fun getCategory(): Category {
        return Category(
            id = 1L,
            name = "Automoveis"
        )
    }
}
package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.UUID

@ExtendWith(SpringExtension::class)
class InsertCategoryServiceTest {

    @field:InjectMocks
    private lateinit var insertCategoryService: InsertCategoryService

    @field:Mock
    private lateinit var categoryRepository: CategoryRepository


    @Test
    fun `must save a category`() {
        // cenario

        val category = Category(
            id = 1L,
            name = "Eletronico"
        )

        val correlationId = UUID.randomUUID().toString()

        // ação

        // comportamento
        Mockito.`when`(categoryRepository.save(ArgumentMatchers.any())).thenReturn(category)

        // assertiva -> nao deve lançar exception
        Assertions.assertDoesNotThrow { insertCategoryService.insertCategory(category, correlationId) }
        Mockito.verify(categoryRepository, Mockito.times(1)).save(ArgumentMatchers.any())
    }
}
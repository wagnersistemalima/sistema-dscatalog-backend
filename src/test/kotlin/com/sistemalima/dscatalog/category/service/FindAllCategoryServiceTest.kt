package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.response.FindAllCategoryResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.List
import java.util.UUID

@ExtendWith(SpringExtension::class)
class FindAllCategoryServiceTest {

    @field:InjectMocks
    private lateinit var findAllCategoryService: FindAllCategoryService

    @Mock  // usar quando a classe de teste não carrega o contexto
    private lateinit var categoryRepository: CategoryRepository

    @Test
    fun `should return paged category list`() {
        // cenario
        val category = factoryCategory()
        val pageRequest = factoryPageRequest()

        // ação

        val list = PageImpl(List.of<Category>(category))

        val response = list.map { category -> FindAllCategoryResponse(category) }

        // comportamento
        Mockito.`when`(categoryRepository.findAll(pageRequest)).thenReturn(list)

        // assertivas
        categoryRepository.findAll(pageRequest)
        Mockito.verify(categoryRepository).findAll(pageRequest)
        Assertions.assertEquals(response, findAllCategoryService.findAllPage(pageRequest, ArgumentMatchers.anyString()))

    }

    private fun factoryCategory(): Category {
        val category = Category(
            id = 1L,
            name = "Computador"
        )
        return category
    }

    private fun factoryPageRequest(): PageRequest {

        val page = 0
        val linesPerPage = 12
        val direction = "ASC"
        val orderBy = "name"

        val pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy)

        return pageRequest

    }
}
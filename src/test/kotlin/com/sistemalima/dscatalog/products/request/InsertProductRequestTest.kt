package com.sistemalima.dscatalog.products.request

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.products.controller.InsertProductController
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.slf4j.LoggerFactory
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.Optional
import java.util.UUID

@ExtendWith(SpringExtension::class)
class InsertProductRequestTest {

    @field:InjectMocks
    private val request = InsertProductRequest(
        name = "Ventilador",
        description = "ventilador preto",
        price = 25.0,
        imgUrl = "img",
        idCategory = 1L
    )

    @field:Mock
    lateinit var categoryRepository: CategoryRepository

    private val logger = LoggerFactory.getLogger(InsertProductController::class.java)


    @Test
    fun `deve converter dados da request em entidade product, nao lancar exception`() {
        // cenario
        val correlationId = UUID.randomUUID().toString()
        val category = Category(
            id = 1L,
            name = "Eletronico"
        )
        val idcategoryValid = category.id

        // ação
        // comportamento
        Mockito.`when`(categoryRepository.findById(idcategoryValid!!)).thenReturn(Optional.of(category))

        // assertivas
        Assertions.assertDoesNotThrow { request.toModel(categoryRepository, correlationId, logger) }
    }

    @Test
    fun `nao deve converter dados da request em entidade product,lancar exception EntityNotFound quando nao achar idCategory`() {
        // cenario
        val correlationId = UUID.randomUUID().toString()

        val idcategoryNaoExiste = 5000L

        // ação
        // comportamento
        Mockito.`when`(categoryRepository.findById(idcategoryNaoExiste)).thenReturn(Optional.empty())

        // assertivas
        Assertions.assertThrows(ResourceNotFoundException::class.java) {request.toModel(
            categoryRepository,
            correlationId,
            logger
        )}
    }

}
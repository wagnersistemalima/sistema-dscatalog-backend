package com.sistemalima.dscatalog.products.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.products.request.InsertProductRequest
import com.sistemalima.dscatalog.products.response.InsertProductResponse
import com.sistemalima.dscatalog.products.service.InsertProductService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.net.URI
import java.util.Optional
import java.util.UUID

@SpringBootTest
@ExtendWith(SpringExtension::class)
@AutoConfigureMockMvc
class InsertProductControllerTest {

    @field:Autowired
    lateinit var mockMvc: MockMvc

    @field:Autowired
    lateinit var objectMapper: ObjectMapper

    @field:MockBean
    lateinit var insertProductService: InsertProductService

    @field:MockBean
    lateinit var categoryRepository: CategoryRepository


    @Test
    fun `deve retornar 201, created quando validar a request`() {
        // cenario

        val requestValid = getRequest()
        val uri = URI(APIConstants.PRODUCT_POST)
        val correlationId = UUID.randomUUID().toString()
        val category = getCategory()

        // ação

        // comportamento
        Mockito.`when`(categoryRepository.findById(requestValid.idCategory)).thenReturn(Optional.of(category))

        val logger = LoggerFactory.getLogger(InsertProductController::class.java)
        val product = requestValid.toModel(categoryRepository, correlationId, logger)
        val response = InsertProductResponse(product)

        // comportamento
        Mockito.`when`(insertProductService.insert(any(), any())).thenReturn(response)

        mockMvc.perform(MockMvcRequestBuilders.post(uri)
            .contentType(MediaType.APPLICATION_JSON)
            .content(tojson(requestValid)))
            .andExpect(MockMvcResultMatchers.status().isCreated)


        //assertivas
    }

    private fun tojson(request: InsertProductRequest): String {
        return objectMapper.writeValueAsString(request)
    }

    fun getRequest(): InsertProductRequest {
        return InsertProductRequest(
            name = "ventilador",
            description = "um ventilador legal",
            price = 20.0,
            imgUrl = "img",
            idCategory = 1L
        )
    }

    fun getCategory(): Category {
        return Category(
            id = 1L,
            name = "Eletronico"
        )
    }
}
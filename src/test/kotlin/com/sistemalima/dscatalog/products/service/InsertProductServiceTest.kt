package com.sistemalima.dscatalog.products.service

import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.products.model.Product
import com.sistemalima.dscatalog.products.repository.ProductRepository
import com.sistemalima.dscatalog.products.response.InsertProductResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.UUID

@ExtendWith(SpringExtension::class)
class InsertProductServiceTest {

    @field:InjectMocks
    lateinit var insertProductService: InsertProductService

    @field:Mock
    lateinit var productRepository: ProductRepository

    @Test
    fun `deve salvar um produto no banco, retornar um response de entidade`() {
        // cenario

        val correlationId = UUID.randomUUID().toString()

        val product = getProduct()

        val response = InsertProductResponse(product)

        // ação

        // comportamento
        Mockito.`when`(productRepository.save(product)).thenReturn(product)

        // assertivas
        Assertions.assertEquals(response, insertProductService.insert(product, correlationId))
    }

    fun getCategory(): Category {
        return Category(
            id = 1L,
            name = "Eletronico"
        )
    }

    fun getProduct(): Product {
        return Product(
            id = 1L,
            name = "Secador",
            description = "um secador legal",
            price = 20.0,
            imgUrl = "img",
            category = getCategory()
        )
    }
}
package com.sistemalima.dscatalog.products.service

import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.products.model.Product
import com.sistemalima.dscatalog.products.repository.ProductRepository
import com.sistemalima.dscatalog.products.response.InsertProductResponse
import org.slf4j.LoggerFactory

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class InsertProductService(
    @field:Autowired val productRepository: ProductRepository
) {

    val logger = LoggerFactory.getLogger(InsertProductService::class.java)

    @Transactional
    fun insert(product: Product, correlationId: String): InsertProductResponse {

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: InsertProductService -> " +
                "method: insert() -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message}, correlationId: $correlationId")

        // implementar validacao para o nome do produto -> produto unico

        productRepository.save(product)

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: InsertProductService -> " +
                "method: insert() -> SUCESS -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message}, correlationId: $correlationId")

        return InsertProductResponse(product)
    }
}
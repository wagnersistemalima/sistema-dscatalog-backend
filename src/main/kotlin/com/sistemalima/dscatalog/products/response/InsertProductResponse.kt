package com.sistemalima.dscatalog.products.response

import com.sistemalima.dscatalog.products.model.Product

data class InsertProductResponse(
    val id: Long?,
    val name: String
){
    constructor(product: Product): this(product.id, product.name)
}

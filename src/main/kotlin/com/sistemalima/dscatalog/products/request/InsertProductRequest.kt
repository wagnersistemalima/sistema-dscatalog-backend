package com.sistemalima.dscatalog.products.request

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.products.model.Product
import org.slf4j.Logger
import org.springframework.transaction.annotation.Transactional
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class InsertProductRequest(
    @field:NotBlank
    val name: String,

    @field:NotBlank
    val description: String,

    @field:NotNull
    @field:Positive
    val price: Double,

    @field:NotBlank
    val imgUrl: String,

    @field:NotNull
    val idCategory: Long
) {

    @Transactional
    fun toModel(categoryRepository: CategoryRepository, correlationId: String, logger: Logger): Product {

        val category = categoryRepository.findById(this.idCategory).orElseThrow {

            logger.error("Error: microservice: Dscatalog -> class: InsertProductRequest -> " +
                    "method: toModel() -> categoryId: ${this.idCategory} -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION} -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION.message}, correlationId: $correlationId")
            throw ResourceNotFoundException("Entity not found!")
        }
        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microsservice: Dscatalog -> class: InsertProductRequest -> " +
                "method: toModel() -> SUCCESS request -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message}, correlationId: $correlationId")

        return Product(
            name = this.name,
            description = this.description,
            price = this.price,
            imgUrl = this.imgUrl,
            category = category
        )
    }
}

package com.sistemalima.dscatalog.products.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.products.request.InsertProductRequest
import com.sistemalima.dscatalog.products.response.InsertProductResponse
import com.sistemalima.dscatalog.products.service.InsertProductService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping(APIConstants.PRODUCT_POST)
class InsertProductController(
    @field:Autowired val insertProductService: InsertProductService,
    @field:Autowired val categoryRepository: CategoryRepository
) {

    val logger = LoggerFactory.getLogger(InsertProductController::class.java)

    @PostMapping
    fun insert(@Valid @RequestBody request: InsertProductRequest): ResponseEntity<InsertProductResponse> {

        val correlationId = UUID.randomUUID().toString()

        logger.info("${ProcessingResult.START_PROCESS} -> " +
                "microsservice: Dscatalog -> class: InsertProductController -> " +
                "method: insert -> HTTP POST -> " +
                "request: $request -> " +
                "${ProcessingResult.START_PROCESS.message}, correlationId: $correlationId")

        val product = request.toModel(categoryRepository, correlationId, logger) // teste unitario
        val response = insertProductService.insert(product, correlationId)

        val uri = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}").buildAndExpand(response.id).toUri()

        return ResponseEntity.created(uri).body(response)

    }
}
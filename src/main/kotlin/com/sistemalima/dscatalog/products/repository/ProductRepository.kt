package com.sistemalima.dscatalog.products.repository

import com.sistemalima.dscatalog.products.model.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository: JpaRepository<Product, Long> {
}
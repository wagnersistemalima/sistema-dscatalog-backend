package com.sistemalima.dscatalog.products.model

import com.sistemalima.dscatalog.category.model.Category
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "tb_product")
class Product(

    @field:Id
    @field:GeneratedValue(strategy = IDENTITY)
    var id: Long? = null,

    val name: String,

    val description: String,

    val price: Double,

    val imgUrl: String,

    @field:ManyToOne // associação -> narios produtos, pertence a uma categoria
    @field:JoinColumn(name = "categoryId")  // chave estrangeira de categoria, na tabela de produtos
    val category: Category
){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Product

        if (id != other.id) return false
        if (name != other.name) return false
        if (description != other.description) return false
        if (price != other.price) return false
        if (imgUrl != other.imgUrl) return false
        if (category != other.category) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + price.hashCode()
        result = 31 * result + imgUrl.hashCode()
        result = 31 * result + category.hashCode()
        return result
    }
}

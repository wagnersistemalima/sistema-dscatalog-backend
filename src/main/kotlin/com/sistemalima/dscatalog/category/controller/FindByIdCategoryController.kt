package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.response.FindByIdCategoryResponse
import com.sistemalima.dscatalog.category.service.FindByIdCategoryService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping(APIConstants.CATEGORY_BY_ID)
class FindByIdCategoryController(
    @field:Autowired val findByIdCategoryService: FindByIdCategoryService
) {

    val logger = LoggerFactory.getLogger(FindByIdCategoryController::class.java)

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long): ResponseEntity<FindByIdCategoryResponse> {
        val correlationId = UUID.randomUUID().toString()

        logger.info(
            "${ProcessingResult.START_PROCESS} -> " +
                    "microservice: Dscatalog -> class: FindBayIdCategoryController ->" +
                    "HTTP GET -> method: findById -> id: $id" +
                    "${ProcessingResult.START_PROCESS.message} -> correlationId: $correlationId"
        )

        val response = findByIdCategoryService.findById(id, correlationId)

        logger.info(
            "${ProcessingResult.START_PROCESS} -> " +
                    "microservice: Dscatalog -> class: FindBayIdCategoryController ->" +
                    "HTTP GET -> method: findById ->" +
                    "return response -> id: $id" +
                    "${ProcessingResult.START_PROCESS.message} -> correlationId: $correlationId"
        )
        return ResponseEntity.ok().body(response)
    }
}
package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.response.FindAllCategoryResponse
import com.sistemalima.dscatalog.category.service.FindAllCategoryService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping(APIConstants.CATEGORY_GET_ALL)
class FindAllCategoryController(
    @field:Autowired val findAllCategoryService: FindAllCategoryService
) {

    val logger = LoggerFactory.getLogger(FindAllCategoryController::class.java)

    @GetMapping
    fun findAllPage(
        @RequestParam(value = "page", defaultValue = "0",) page: Int,
        @RequestParam(value = "linesPerPage", defaultValue = "12") linesPerPage: Int,
        @RequestParam(value = "direction", defaultValue = "ASC") direction: String,
        @RequestParam(value = "orderBy", defaultValue = "name") orderBy: String
    ): ResponseEntity<Page<FindAllCategoryResponse>> {
        val correlationId = UUID.randomUUID().toString()
        logger.info("${ProcessingResult.START_PROCESS} -> " +
                "microservice: Dscatalog -> class: FindAllCategoryController ->" +
                "HTTP GET -> method: findAllPage -> " +
                "${ProcessingResult.START_PROCESS.message} -> correlationId: $correlationId")

        val pageRequest = PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy)
        val response = findAllCategoryService.findAllPage(pageRequest, correlationId)
        return ResponseEntity.ok().body(response)
    }
}
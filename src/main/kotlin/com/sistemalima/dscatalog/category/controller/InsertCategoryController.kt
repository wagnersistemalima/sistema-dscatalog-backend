package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.request.InsertCategoryRequest
import com.sistemalima.dscatalog.category.response.InsertCategoryResponse
import com.sistemalima.dscatalog.category.service.InsertCategoryService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping(APIConstants.CATEGORY_POST)
class InsertCategoryController(
    @field:Autowired val insertCategoryService: InsertCategoryService
) {

    val logger = LoggerFactory.getLogger(InsertCategoryController::class.java)

    @PostMapping
    fun insertCategory(@Valid @RequestBody request: InsertCategoryRequest): ResponseEntity<InsertCategoryResponse> {
        val correlationId = UUID.randomUUID().toString()

        logger.info("${ProcessingResult.START_PROCESS} ->" +
                "microservice: Dscatalog -> class: InsertProductController ->" +
                "HTTP POST -> method: insertCategory ->" +
                "${ProcessingResult.START_PROCESS.message}, correlationId: $correlationId")
        val category = request.toModel()
        val response = insertCategoryService.insertCategory(category, correlationId)
        val uri = ServletUriComponentsBuilder.fromCurrentRequest()
            .path("/{id}").buildAndExpand(response.id).toUri()

        logger.info("${ProcessingResult.END_PROCESS} ->" +
                "microservice: Dscatalog -> class: InsertProductController ->" +
                "HTTP POST -> method: insertCategory ->" +
                "${ProcessingResult.END_PROCESS.message}, correlationId: $correlationId")

        return ResponseEntity.created(uri).body(response)
    }
}
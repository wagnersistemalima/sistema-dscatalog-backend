package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.service.DeleteCategoryService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping(APIConstants.CATEGORY_DELETE_BY_ID)
class DeleteCategoryController(
    @field:Autowired val deleteCategoryService: DeleteCategoryService
) {

    val logger = LoggerFactory.getLogger(DeleteCategoryController::class.java)

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: Long): ResponseEntity<Unit> {

        val correlationId = UUID.randomUUID().toString()
        logger.info("${ProcessingResult.START_PROCESS} -> " +
                "microservice: Dscatalog -> class: DeleteCategoryController -> " +
                "method: delete -> HTTP DELETE -> idCategory: $id -> " +
                "${ProcessingResult.START_PROCESS.message}, correlationId: $correlationId")

        deleteCategoryService.delete(id, correlationId)

        logger.info("${ProcessingResult.END_PROCESS} -> " +
                "microservice: Dscatalog -> class: DeleteCategoryController -> " +
                "method: delete -> HTTP DELETE -> idCategory: $id -> SUCCESS, " +
                "${ProcessingResult.END_PROCESS.message}, correlationId: $correlationId")

        return ResponseEntity.noContent().build()
    }
}
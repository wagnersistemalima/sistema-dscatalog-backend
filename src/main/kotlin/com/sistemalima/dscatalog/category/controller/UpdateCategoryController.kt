package com.sistemalima.dscatalog.category.controller

import com.sistemalima.dscatalog.category.constants.APIConstants
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.request.UpdateCategoryRequest
import com.sistemalima.dscatalog.category.service.UpdateCategoryService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID
import javax.validation.Valid

@Validated
@RestController
@RequestMapping(APIConstants.CATEGORY_BY_ID_UPADATE)
class UpdateCategoryController(
    @field:Autowired val updateCategoryService: UpdateCategoryService
) {

    val logger = LoggerFactory.getLogger(UpdateCategoryController::class.java)

    @PutMapping("/{id}")
    fun update(@PathVariable("id") id: Long, @Valid @RequestBody request: UpdateCategoryRequest): ResponseEntity<Unit> {

        val correlationId = UUID.randomUUID().toString()
        logger.info(
            "${ProcessingResult.START_PROCESS} -> " +
                    "microservice: Dscatalog -> class: UpdateCategoryController ->" +
                    "HTTP PUT -> method: update -> idCategory: $id" +
                    "${ProcessingResult.START_PROCESS.message} -> correlationId: $correlationId"
        )
        updateCategoryService.update(id, request, correlationId)
        return ResponseEntity.ok().build()
    }
}
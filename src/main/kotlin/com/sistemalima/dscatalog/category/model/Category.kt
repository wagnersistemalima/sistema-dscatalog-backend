package com.sistemalima.dscatalog.category.model

import com.sistemalima.dscatalog.products.model.Product
import java.time.LocalDateTime
import javax.persistence.CascadeType.ALL
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType.IDENTITY
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "tb_category")
class Category(
    @Id
    @GeneratedValue(strategy = IDENTITY)
    val id: Long? = null,
    var name: String
){
    val recordDate = LocalDateTime.now()
    var updateDate: LocalDateTime? = null

    // propaga todas as açoes para seus dependentes
    @field:OneToMany(cascade = arrayOf(ALL), mappedBy = "category", orphanRemoval = true)
    val products = mutableListOf<Product>()

    fun updateDate() {
        updateDate = LocalDateTime.now()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Category

        if (id != other.id) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + name.hashCode()
        return result
    }
}

package com.sistemalima.dscatalog.category.repository

import com.sistemalima.dscatalog.category.model.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository: JpaRepository<Category, Long> {
}
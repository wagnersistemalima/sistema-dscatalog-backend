package com.sistemalima.dscatalog.category.response

import com.sistemalima.dscatalog.category.model.Category

data class FindAllCategoryResponse(
    val id: Long?,
    val name: String
){
    constructor(category: Category): this(category.id, category.name)
}

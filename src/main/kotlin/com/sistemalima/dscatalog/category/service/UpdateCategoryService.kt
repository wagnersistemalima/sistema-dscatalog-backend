package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.request.UpdateCategoryRequest
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UpdateCategoryService(
    @field:Autowired val categoryRepository: CategoryRepository
) {

    val logger = LoggerFactory.getLogger(UpdateCategoryService::class.java)

    @Transactional
    fun update(id: Long, request: UpdateCategoryRequest, correlationId: String) {
        logger.info(
            "${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                    "microservice: Dscatalog -> class: UpdateCategoryService ->" +
                    "method: update -> idCategory: $id ->" +
                    "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> correlationId: $correlationId"
        )

        val category = categoryRepository.findById(id).orElseThrow {

            logger.error("Error: microservice: Dscatalog -> class: UpdateCategoryService -> " +
                    "method: update -> idCategory: $id -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION} ->" +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION.message}, coorelationId: $correlationId")

            throw ResourceNotFoundException("Entity not found")
        }

        category.name = request.name
        category.updateDate()
        categoryRepository.save(category)
    }
}
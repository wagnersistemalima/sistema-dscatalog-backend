package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DeleteCategoryService(
    @field:Autowired val categoryRepository: CategoryRepository
) {
    val logger = LoggerFactory.getLogger(DeleteCategoryService::class.java)

    @Transactional
    fun delete(id: Long, correlationId: String) {

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: DeleteCategoryService -> " +
                "method: delete -> idCategory: $id -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> correlationId: $correlationId")

        val category = categoryRepository.findById(id).orElseThrow {

            logger.error("Error: microservice: Dscatalog -> class: DeleteCategoryService -> " +
                    "method: delete -> idCategory: $id -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION} -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION.message}, correlationId: $correlationId")

            throw ResourceNotFoundException("Entity not found!")
        }

        categoryRepository.delete(category)

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: DeleteCategoryService -> " +
                "method: delete -> idCategory: $id -> SUCCESS, " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> correlationId: $correlationId")
    }
}
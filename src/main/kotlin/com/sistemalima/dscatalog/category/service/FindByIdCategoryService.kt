package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.advice.exceptions.ResourceNotFoundException
import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.response.FindByIdCategoryResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import kotlin.math.log

@Service
class FindByIdCategoryService(
    val categoryRepository: CategoryRepository
) {
    val logger = LoggerFactory.getLogger(FindByIdCategoryService::class.java)

    @Transactional
    fun findById(id: Long, correlationId: String): FindByIdCategoryResponse {

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: FindAllCategoryService ->" +
                "method: findById -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message}, correlationId: $correlationId")

        val category = categoryRepository.findById(id).orElseThrow {

            logger.error("Error: microservice: Dscatalog -> class: FindAllCategoryService -> " +
                    "method: findById -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION} -> " +
                    "${ProcessingResult.ENTITY_NOT_FOUND_EXCEPTION.message}, correlationId: $correlationId")

            throw ResourceNotFoundException("Entity not found")
        }
        val response = FindByIdCategoryResponse(category)

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: FindAllCategoryService ->" +
                "method: findById -> " +
                "return response categoory -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message}, correlationId: $correlationId")
        return response
    }
}
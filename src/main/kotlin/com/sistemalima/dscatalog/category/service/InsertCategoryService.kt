package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.model.Category
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.response.InsertCategoryResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class InsertCategoryService(
    @field:Autowired val categoryRepository: CategoryRepository
) {

    val logger = LoggerFactory.getLogger(InsertCategoryService::class.java)

    @Transactional
    fun insertCategory(category: Category, coorelationId: String): InsertCategoryResponse {
        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: InsertCategoryService -> " +
                "method: insertCategory ->" +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> " +
                "correlationId: $coorelationId")

        categoryRepository.save(category)
        return InsertCategoryResponse(category)
    }
}
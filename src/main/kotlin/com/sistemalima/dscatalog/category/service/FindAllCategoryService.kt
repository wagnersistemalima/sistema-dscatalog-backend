package com.sistemalima.dscatalog.category.service

import com.sistemalima.dscatalog.category.constants.ProcessingResult
import com.sistemalima.dscatalog.category.repository.CategoryRepository
import com.sistemalima.dscatalog.category.response.FindAllCategoryResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class FindAllCategoryService(
    @field:Autowired val categoryRepository: CategoryRepository
) {

    val logger = LoggerFactory.getLogger(FindAllCategoryService::class.java)

    @Transactional
    fun findAllPage(pageRequest: PageRequest, correlationId: String): Page<FindAllCategoryResponse> {

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: FindAllCategoryService ->" +
                "method: findAllPage ->" +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> correlationId: $correlationId")

        val list = categoryRepository.findAll(pageRequest)
        val response = list.map { category -> FindAllCategoryResponse(category) }

        logger.info("${ProcessingResult.GET_MOVIMENT_NUMBER} -> " +
                "microservice: Dscatalog -> class: FindAllCategoryService ->" +
                "method: findAllPage -> " +
                "paged return performed successfully -> " +
                "${ProcessingResult.GET_MOVIMENT_NUMBER.message} -> correlationId: $correlationId")

        return response
    }
}
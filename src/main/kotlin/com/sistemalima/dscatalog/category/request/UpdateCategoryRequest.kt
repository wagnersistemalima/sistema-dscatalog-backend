package com.sistemalima.dscatalog.category.request

import javax.validation.constraints.NotBlank

data class UpdateCategoryRequest(

    @field:NotBlank
    val name: String
)


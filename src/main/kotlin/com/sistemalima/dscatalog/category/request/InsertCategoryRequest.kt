package com.sistemalima.dscatalog.category.request

import com.sistemalima.dscatalog.category.model.Category
import javax.validation.constraints.NotBlank

data class InsertCategoryRequest(
    @field:NotBlank
    val name: String
) {
    fun toModel(): Category {
        return Category(name = name)
    }
}

package com.sistemalima.dscatalog.category.constants

class APIConstants {

    companion object {
        const val CATEGORY_POST = "/api/categories"
        const val CATEGORY_GET_ALL = "/api/categories"
        const val CATEGORY_BY_ID = "/api/categories"
        const val CATEGORY_BY_ID_UPADATE = "/api/categories"
        const val CATEGORY_DELETE_BY_ID = "/api/categories"
        const val PRODUCT_POST = "/api/products"

    }
}
package com.sistemalima.dscatalog.category.advice

data class FieldMessage(
    val fieldName: String,
    val message: String
)

package com.sistemalima.dscatalog.category.advice.exceptions

class ResourceNotFoundException(message: String): RuntimeException(message) {
}